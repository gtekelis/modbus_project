var reader = require('./meterRead');

var express = require('express');
var path = require('path');
var app = express();
var winston = require('winston');
var fs = require('fs');
var config = require('./config');

app.use(express.static('public'));
app.set('views', path.join(__dirname, 'views'));


var logDir = "./logs"
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

var tsFormat = function () { return (new Date()).toLocaleTimeString(); }

var findLevel = function () {
    if (!process.env.NODE_ENV) {
        //return process.env === 'development' ? 'verbose' : 'info'
        return 'debug';
    }
    else {
        //return process.env.NODE_ENV === 'development' ? 'verbose' : 'info'
        return 'debug';
    }

}

var logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            timestamp: tsFormat,
            colorize: true,
            level: 'debug'
        }),
        new (require("winston-daily-rotate-file"))({
            filename: `${logDir}/-results.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: findLevel()
        })
    ]
});

//routes
app.get('/', function (req, res) {
    res.send('modbus api');
});

app.get('/index', function (req, res) {
    res.sendFile(path.resolve("./views/index.html"));
})


app.get('/getAllValues', function (req, res) {

    reader.meterRead(config.ip, config.port, logger)
        .then(
        function fulfilled(vals) {
            // var retObj = populateReturnObject(vals);

            logger.log('from api ' + JSON.stringify(vals));
            logger.info("Received data from modbus")
            //publishOne(retObj);
            res.send(vals);
            //return vals;
        },
        function rej(err) {
            logger.error(err);
        }
        );
});

app.get('/acSet', function (req, res) {

    var acState = req.param('param1') == 'true' ? true : false;
    logger.log('set ac value ' + acState);

    reader.meterWriteAc(config.ip, config.port, logger, acState)
    .then(function () {
        res.send('ok');
    },function(err){
        res.status(500).send('cannot write to modbus' + err);
    })
    .catch(function(err){
        logger.error(JSON.stringify(err));
        res.sendStatus(500);
    });

});


app.get('/heaterSet', function (req, res) {

    var heaterState = req.param('param2') == 'true' ? true : false;
    logger.log('set heat value ' + heaterState);

    reader.meterWriteHeater(config.ip, config.port, logger, heaterState).then(function () {
        res.send('ok');
    },function(err){
        res.status(500).send('cannot write to modbus' + err);
    })
    .catch(function(err){
        logger.error(JSON.stringify(err));
        res.sendStatus(500);
    });;

});



var server = app.listen(8081, function () {

    var host = server.address().address
    var port = server.address().port

    logger.log("Example app listening at http://%s:%s", host, port)

})