var myApp = angular.module('myApp', ['frapontillo.bootstrap-switch']);

var isStarting = true;

var stateAc = null;
var stateHeater = null;

myApp.controller('AppCtrl', function ($scope, $http, $timeout, $interval, $rootScope) {


  function setAc(param) {
    $http({
      method: 'GET',
      url: '/acSet',
      params: {
        param1: param
      }
    }).then(function successCallback(response) {
      console.log('ac changed ');
      // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
      console.log('error set ac');
    });
  };



  function setHeat(param2) {
    $http({
      method: 'GET',
      url: '/heaterSet',
      params: {
        param2: param2
      }
    }).then(function successCallback(response) {
      console.log('heater changed ');
      // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
      console.log('error set heater');
    });
  };


  // jQuery('#ac_on').on('click', function () {
  //   //$(this).button('complete') // button text will be "finished!"
  //   console.log('ac ON clicked');
  //   setAc(true);
  // });

  // jQuery('#ac_off').on('click', function () {
  //   //$(this).button('complete') // button text will be "finished!"
  //   console.log('ac OFF clicked');
  //   setAc(false);
  // });




  // jQuery('#heater_on').on('click', function () {
  //   //$(this).button('complete') // button text will be "finished!"
  //   console.log('heater ON clicked');
  //   setHeat(true);
  // });

  // jQuery('#heater_off').on('click', function () {
  //   //$(this).button('complete') // button text will be "finished!"
  //   console.log('ac OFF clicked');
  //   setHeat(false);
  // });



  jQuery("[name='ac-checkbox']").bootstrapSwitch();
  jQuery("[name='heat-checkbox']").bootstrapSwitch();

  jQuery('input[name="ac-checkbox"]').on('switchChange.bootstrapSwitch', function (event, state) {
    // console.log(this); // DOM element
    // console.log(event); // jQuery event
    // console.log(state); // true | false
    //$scope.allValues.ac = state;
    //$scope.$apply();
    setAc(state);
    console.log('ac switch change called');
    //setAc();
  });

  jQuery('input[name="heat-checkbox"]').on('switchChange.bootstrapSwitch', function (event, state) {
    // console.log(this); // DOM element
    // console.log(event); // jQuery event
    // console.log(state); // true | false
    // $scope.allValues.heater = state;
    // $scope.$apply();
    setHeat(state);
    console.log('heat switch change called');

  });



  $interval(function () {
    console.log('all vals');
    getAllValues();
  }, 8000);


  function getAllValues() {

    $http.get('/getAllValues')
      .success(function (data) {
        $scope.allValues = data;
        //set initial state

        // if (isStarting) {
        // isStarting = false;

        //}
        stateAc = data.ac;
        stateHeater = data.heater

        console.log(JSON.stringify(data));


         jQuery('input[name="ac-checkbox"]').bootstrapSwitch('state', stateAc, true);
         jQuery('input[name="heat-checkbox"]').bootstrapSwitch('state', stateHeater, true);


      })
      .error(function (data) {
        console.log('Error: ' + JSON.stringify(data));
      });
  }


});