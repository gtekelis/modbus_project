/// Module meterRead.js ///
/// Reads all the values from modbus and returns them in an object
var modbus = require('jsmodbus');
//setup the decoder for BE single precision values
var BitsAndBytes = require('bitsandbytes');
var decoder = new BitsAndBytes();

// wrap the modbus connection to a promise
function connectPromise(cl, logger) {
    return new Promise(
        function (resolve, reject) {
            cl.on('connect', function () {
                logger.info("Modbus Connected!")    
                resolve(null);
            });
            cl.on("error", function (err) {
                logger.error("ConnectPromise error");
                reject(err);
            });

            cl.connect();
        }
    )
}

//decodes the 4 byte BE IEEE 754 to single
function decodeToFloat(buf, decimalpoints) {
    var val = decoder.decodeBE(buf, { type: 'float', start: 0 });
    return val.toFixed(decimalpoints);
}

//decodes boolean coils
function decodeToBoolean(coil) {
    var retVal = false;
    coil.forEach(function (b) { retVal = retVal || b });
    return retVal;
}

function getCurrentDate() {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = dd + '/' + mm + '/' + yyyy;
    return today;
    //document.write(today);

}

function read(ip, port, logger) {
    var decoder = new BitsAndBytes();
    var options = {
        'host': ip,
        'port': port,
        'autoReconnect': false,
        //'reconnectTimeout'  : 1000,
        //'timeout'           : 5000,
        'unitId': 0
    }

    // create a modbus client
    var client = modbus.client.tcp.complete(options);
    //return thenable
    return new Promise(
        function (resolve, reject) {
            connectPromise(client, logger)
                .then(
                function () {
                    var promises = [];
                    promises.push(
                        client.readInputRegisters(4, 2), //voltage
                        client.readInputRegisters(22, 2), //Power
                        client.readCoils(0, 1), // AC
                        client.readCoils(1, 1),  //heater
                        client.readInputRegisters(10, 2), // curr_L1
                        client.readInputRegisters(28, 2), // rpower
                        client.readInputRegisters(34, 2) // pf_L1
                    );
                    Promise.all(promises)
                        .then(
                        function (vals) {
                            logger.log("meterRead:/n" + JSON.stringify(vals));
                            var obj =
                                {
                                    ac: vals[2].coils[0],
                                    heater: decodeToBoolean(vals[3].coils),
                                    volt: decodeToFloat(vals[0].payload, 3),
                                    power: decodeToFloat(vals[1].payload, 3),
                                    curr: decodeToFloat(vals[4].payload, 3),
                                    rpower: decodeToFloat(vals[5].payload, 3),
                                    pf: decodeToFloat(vals[6].payload, 3),
                                    date: getCurrentDate()
                                }
                            client.close();
                            resolve(obj);
                        }
                        )
                        .catch(
                        function (err) {
                            client.close();
                            //console.error("All promises");
                            logger.error("All promises" + err);
                            reject(err);
                        }
                        )
                }
                ) // connect
                .catch(
                function (err) {
                    logger.error("meterRead cannot connect");
                    client.close();
                    reject(err);
                }
                )
        }//promise function
    )//Promise

}

function write(ip, port, logger, state, writeTo){
    var decoder = new BitsAndBytes();
    var options = {
        'host': ip,
        'port': port,
        'autoReconnect': false,
        //'reconnectTimeout'  : 1000,
        //'timeout'           : 5000,
        'unitId': 0
    }

    // create a modbus client
    var client = modbus.client.tcp.complete(options);

    //return thenable
    return new Promise(
        function (resolve, reject) {
            connectPromise(client, logger)
                .then(
                function () {
                    var promises = [];
                    promises.push(
                        client.writeSingleCoil(writeTo == 'ac' ? 0 : 1, state)
                    );
                    Promise.all(promises)
                        .then(
                        function (vals) {
                            logger.log("meterRead:/n" + JSON.stringify(vals));
                           
                            client.close();
                            resolve(true);
                        }
                        )
                        .catch(
                        function (err) {
                            client.close();
                            //console.error("All promises");
                            logger.error("All promises" + err);
                            reject(err);
                        }
                        )
                }
                ) // connect
                .catch(
                function (err) {
                    logger.error("meterWrite cannot connect");
                    client.close();
                    reject(err);
                }
                )
        }//promise function
    )//Promise

}

function writeAc(ip, port, logger, state){
    var decoder = new BitsAndBytes();
    var options = {
        'host': ip,
        'port': port,
        'autoReconnect': false,
        //'reconnectTimeout'  : 1000,
        //'timeout'           : 5000,
        'unitId': 0
    }

    // create a modbus client
    var client = modbus.client.tcp.complete(options);

    //return thenable
    return new Promise(
        function (resolve, reject) {
            connectPromise(client, logger)
                .then(
                function () {
                    var promises = [];
                    promises.push(
                        client.writeSingleCoil(0, state)
                    );
                    Promise.all(promises)
                        .then(
                        function (vals) {
                            //console.log("meterRead:/n" + JSON.stringify(vals));
                           
                            client.close();
                            resolve(true);
                        }
                        )
                        .catch(
                        function (err) {
                            client.close();
                            //console.error("All promises");
                            logger.error("All promises" + err);
                            reject(err);
                        }
                        )
                }
                ) // connect
                .catch(
                function (err) {
                    logger.error("meterWrite cannot connect");
                    client.close();
                    reject(err);
                }
                )
        }//promise function
    )//Promise

}

function writeHeater(ip, port, logger, state){
    var decoder = new BitsAndBytes();
    var options = {
        'host': ip,
        'port': port,
        'autoReconnect': false,
        //'reconnectTimeout'  : 1000,
        //'timeout'           : 5000,
        'unitId': 0
    }

    // create a modbus client
    var client = modbus.client.tcp.complete(options);

    //return thenable
    return new Promise(
        function (resolve, reject) {
            connectPromise(client, logger)
                .then(
                function () {
                    var promises = [];
                    promises.push(
                        client.writeSingleCoil(1, state)
                    );
                    Promise.all(promises)
                        .then(
                        function (vals) {
                            logger.log("meterRead:/n" + JSON.stringify(vals));
                           
                            client.close();
                            resolve(true);
                        }
                        )
                        .catch(
                        function (err) {
                            client.close();
                            //logger.error("All promises");
                            logger.error("All promises" + err);
                            reject(err);
                        }
                        )
                }
                ) // connect
                .catch(
                function (err) {
                    logger.error("meterWrite cannot connect");
                    client.close();
                    reject(err);
                }
                )
        }//promise function
    )//Promise

}

module.exports.meterRead = read;
module.exports.meterWrite = write;
module.exports.meterWriteAc = writeAc;
module.exports.meterWriteHeater = writeHeater;